package com.cognizant.LearnTodayRESTAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.LearnTodayRESTAPI.dao.CourseDao;
import com.cognizant.LearnTodayRESTAPI.dao.impl.CourseDaoImpl;
import com.cognizant.LearnTodayRESTAPI.model.Course;
import com.cognizant.LearnTodayRESTAPI.service.AdminService;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
	
	
	AdminService service=new AdminService();
	
	
	@GetMapping("/getAllCourses")
	public ResponseEntity<Object> getAllCourses(){
		
		List<Course> courses =  service.getAllCourses();
		if(courses.size()==0) {
			return new ResponseEntity<>("No courses found", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(courses, HttpStatus.OK);
	}
	
	@GetMapping("/getCourseById/{courseId}")
	public ResponseEntity<Object> getCourseById(@PathVariable int courseId){
		
		List<Course> courses = service.getCourseById(courseId);
		if(courses.isEmpty()) {
			return new ResponseEntity<>("Searched Data not found", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(courses, HttpStatus.OK);
	}
}
