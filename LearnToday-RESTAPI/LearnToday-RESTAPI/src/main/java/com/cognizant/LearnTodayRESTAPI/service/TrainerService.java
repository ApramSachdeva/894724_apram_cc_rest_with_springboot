package com.cognizant.LearnTodayRESTAPI.service;

import com.cognizant.LearnTodayRESTAPI.dao.TrainerDao;
import com.cognizant.LearnTodayRESTAPI.dao.exception.InvalidTrainerIdException;
import com.cognizant.LearnTodayRESTAPI.dao.impl.TrainerDaoImpl;
import com.cognizant.LearnTodayRESTAPI.model.Trainer;

public class TrainerService {

	private TrainerDao dao=new TrainerDaoImpl();
	
	
	public int signUp(Trainer t) {
		return dao.signUp(t);
	}
	public int updatePassWord(int id,Trainer t) throws InvalidTrainerIdException {
		return dao.updatePassWord(id,t);
	}
}
